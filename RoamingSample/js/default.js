﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232509
(function () {
    "use strict";

    var app = WinJS.Application;
    app.ROAMING_CANVAS_FILENAME = 'IMissIStream';   // Filename for our unstructured state (canvas pixel data)
    app.canvas;                                     // Larger canvas for drawing on
    app.ctx;                                        // 2D context for the drawing canvas
    app.colorPickerCanvas;                          // Smaller canvas for the simple color picker
    app.painting = false;                           // Flag to holding painting state
    app.paintingColor = 'black';                    // Current painting color (default to black)
    app.clickX = new Array();                       // [] of x coords while drawing
    app.clickY = new Array();                       // [] of y coords while drawing
    app.clickDrag = new Array();                    // [] of drag state (click and holding while moving on the canvas)
    app.canvasLoaded = false;                       // Flag used to prevent new input while the canvas state is loading

    // Helper to find a DOM element nearly every sample I've seen uses
    function id (elementId) {
        return document.getElementById(elementId);
    }

    app.onactivated = function (eventObject) {
        if (eventObject.detail.kind === Windows.ApplicationModel.Activation.ActivationKind.launch) {
            if (eventObject.detail.previousExecutionState !== Windows.ApplicationModel.Activation.ApplicationExecutionState.terminated) {
                // This application has been newly launched.
                app.initState();
            } else {
                // This application has been reactivated from suspension. 
                // Restore application state here.
                app.initState();
            }
            WinJS.UI.processAll();
        }
    };

    app.initState = function () {
        id('roamingCounterIncButton').addEventListener('click', app.onClickButton, false);
        id('roamingCounterDecButton').addEventListener('click', app.onClickButton, false);
        id('roamingStringButton').addEventListener('click', app.onClickButton, false);
        id('saveCanvasButton').addEventListener('click', app.onClickButton, false);
        id('clearCanvasButton').addEventListener('click', app.onClickButton, false);
        id('roamingStringInput').addEventListener('input', app.onStringInput, false);

        app.canvas = document.getElementById('canvas');
        app.canvas.addEventListener('MSPointerDown', app.processCanvasDown, false);
        app.canvas.addEventListener('MSPointerUp', app.processCanvasUp, false);
        app.canvas.addEventListener('MSPointerMove', app.processCanvasMove, false);
        app.ctx = app.canvas.getContext('2d');

        app.colorPickerCanvas = id('canvasColorPicker');
        app.colorPickerCanvas.addEventListener('MSPointerUp', app.processColorPickerUp, false);

        var appData = Windows.Storage.ApplicationData.current;
        appData.addEventListener('datachanged', app.onRoamingDataChanged);

        app.updatedColorPicker(0);
        app.clearCanvas();

        // Fill in some defaults if the settings are missing
        var appData = Windows.Storage.ApplicationData.current;
        if (!appData.roamingSettings.values.hasOwnProperty('roamedString')) {
            appData.roamingSettings.values.insert('roamedString', '');
        }

        if (!appData.roamingSettings.values.hasOwnProperty('roamedCounter')) {
            appData.roamingSettings.values.insert('roamedCounter', 0);
        }

        app.UpdateUIStructured();
        app.readCanvasFromFile();
    }

    app.oncheckpoint = function (eventObject) {
        // TODO: This application is about to be suspended. Save any state
        // that needs to persist across suspensions here. You might use the 
        // WinJS.Application.sessionState object, which is automatically
        // saved and restored across suspension. If you need to complete an
        // asynchronous operation before your application is suspended, call
        // eventObject.setPromise().
        
        // Since all of the roaming state for this test app is set explicitly on a button press
        // We don't need to save it here, but normally this is where we'd do it
    };

    app.updatedColorPicker = function (selectedX) {
        // This doesn't redraw often, so just get the context each time
        var colorPickerCtx = app.colorPickerCanvas.getContext('2d');

        // Erase the canvas
        colorPickerCtx.fillStyle = 'white';
        colorPickerCtx.beginPath();
        colorPickerCtx.rect(0, 0, app.colorPickerCanvas.width, app.colorPickerCanvas.height);
        colorPickerCtx.closePath();
        colorPickerCtx.fill();

        // Fill in the color options, offset by 1 pixel on each edge so there's an outline of white
        var colors = ['black', 'white', 'red', 'green', 'blue'];
        for (var i = 0; i < colors.length; i++) {
            colorPickerCtx.fillStyle = colors[i];
            colorPickerCtx.beginPath();
            colorPickerCtx.rect((i * (app.colorPickerCanvas.width / colors.length)) + 1, 1, (app.colorPickerCanvas.width / colors.length) - 2, app.colorPickerCanvas.height - 2);
            colorPickerCtx.closePath();
            colorPickerCtx.fill();
        }

        // Draw a selection rectangle over the current drawing color
        var selectedColor = Math.floor(selectedX / (app.colorPickerCanvas.width / colors.length));
        if (selectedColor >= 0 && selectedColor < colors.length) {
            colorPickerCtx.strokeStyle = 'magenta';
            colorPickerCtx.lineWidth = 5;
            colorPickerCtx.beginPath();
            colorPickerCtx.strokeRect((selectedColor * (app.colorPickerCanvas.width / colors.length)), 0, (app.colorPickerCanvas.width / colors.length), app.colorPickerCanvas.height);
            colorPickerCtx.closePath();
            colorPickerCtx.fill();
            app.paintingColor = colors[selectedColor];
        }
    }

    app.onStringInput = function () {
        // Cap to a max length
        var node = id('roamingStringInput');
        var v = node.value;
        if (v.length > 30) {
            node.value = v.substring(0, 30);
        }
    }

    app.onClickButton = function (e) {
        var appData = Windows.Storage.ApplicationData.current;
        if (e.target === id('roamingCounterIncButton')) {
            var i = appData.roamingSettings.values['roamedCounter'] + 1;
            appData.roamingSettings.values.insert('roamedCounter', i);
            app.UpdateUIStructured();
        } else if (e.target === id('roamingCounterDecButton')) {
            var i = appData.roamingSettings.values['roamedCounter'] - 1;
            appData.roamingSettings.values.insert('roamedCounter', i);
            app.UpdateUIStructured();
        } else if (e.target === id('roamingStringButton')) {
            appData.roamingSettings.values['roamedString'] = id('roamingStringInput').value;
            app.UpdateUIStructured();
        } else if (e.target === id('saveCanvasButton')) {
            app.writeCanvasToFile();
        } else if (e.target === id('clearCanvasButton')) {
            app.clickDrag.length = 0;
            app.clickX.length = 0;
            app.clickY.length = 0;
            app.clearCanvas();
            app.writeCanvasToFile();
        }
    }

    app.processColorPickerUp = function (e) {
        app.updatedColorPicker(e.currentPoint.position.x);
    }

    app.processCanvasDown = function (e) {
        if (app.canvasLoaded) {
            app.painting = true;
            app.addClick(e.currentPoint.position.x, e.currentPoint.position.y);
            app.redrawCanvas();
        }
    }

    app.processCanvasUp = function (e) {
        app.painting = false;
    }

    app.processCanvasMove = function (e) {
        if (app.painting) {
            app.addClick(e.offsetX, e.offsetY, true);
            app.redrawCanvas();
        }
    }

    app.addClick = function (x, y, dragging) {
        app.clickX.push(x);
        app.clickY.push(y);
        app.clickDrag.push(dragging);
    }

    app.redrawCanvas = function () {
        app.ctx.save();
        app.ctx.strokeStyle = app.paintingColor;
        app.ctx.lineJoin = 'round';
        app.ctx.lineWidth = 2;
        for (var i = 0; i < app.clickX.length; i++) {
            app.ctx.beginPath();
            if (app.clickDrag[i] && i) {
                app.ctx.moveTo(app.clickX[i - 1], app.clickY[i - 1]);
            } else {
                app.ctx.moveTo(app.clickX[i] - 1, app.clickY[i]);
            }
            app.ctx.lineTo(app.clickX[i], app.clickY[i]);
            app.ctx.closePath();
        }
        app.ctx.stroke();
        app.ctx.restore();
    }

    app.clearCanvas = function () {
        app.ctx.save();
        app.ctx.fillStyle = 'white';
        app.ctx.beginPath();
        app.ctx.rect(0, 0, app.canvas.width, app.canvas.height);
        app.ctx.closePath();
        app.ctx.fill();
        app.ctx.restore();
    }

    app.onRoamingDataChanged = function (evt) {
        app.UpdateUIStructured();
        app.UpdateUIUnstructured();
    }

    // Updates displayed HTML with the current structured roamed settings
    app.UpdateUIStructured = function () {
        app.canvasLoaded = false;
        var appData = Windows.Storage.ApplicationData.current;
        id('divRoamingStringValue').innerHTML = '<p>String: ' + '\"' + appData.roamingSettings.values['roamedString'] + '\"' + '</p>';
        id('divRoamingCounterValue').innerHTML = '<p>Counter: ' + '\'' + appData.roamingSettings.values['roamedCounter'] + '\'' + '</p>';
    }

    app.UpdateUIUnstructured = function () {
        app.readCanvasFromFile();
    }

    app.readCanvasFromFile = function () {
        var roamingFolder = Windows.Storage.ApplicationData.current.roamingFolder;
        roamingFolder.createFileAsync(app.ROAMING_CANVAS_FILENAME,  Windows.Storage.CreationCollisionOption.openIfExists).then(function (file) {
            file.openAsync(Windows.Storage.FileAccessMode.read).then(function (stream) {
                var size = stream.size;
                if (size === 0) {
                    // Canvas is blank
                    app.clearCanvas();
                    app.canvasLoaded = true;
                } else {
                    var inputStream = stream.getInputStreamAt(0);
                    var decompressor = new Windows.Storage.Compression.Decompressor(inputStream);
                    var dataStream = new Windows.Storage.Streams.InMemoryRandomAccessStream();

                    Windows.Storage.Streams.RandomAccessStream.copyAsync(decompressor, dataStream).then(function () {
                        decompressor.detachStream();
                        decompressor.close();
                        if (inputStream) {
                            inputStream.close();
                        }

                        dataStream.seek(0);
                        var reader = new Windows.Storage.Streams.DataReader(dataStream);
                        reader.loadAsync(dataStream.size).then(function () {
                            var contents = []
                            contents.length = dataStream.size;
                            reader.readBytes(contents);

                            var imageData = app.ctx.createImageData(app.canvas.width, app.canvas.height);
                            for (var y = 0; y < app.canvas.height; y++) {
                                for (var x = 0; x < app.canvas.width; x++) {
                                    // Pixels are RGBA
                                    imageData.data[(y * app.canvas.width + x) * 4] = contents[(y * app.canvas.width + x) * 4];
                                    imageData.data[(y * app.canvas.width + x) * 4 + 1] = contents[(y * app.canvas.width + x) * 4 + 1];
                                    imageData.data[(y * app.canvas.width + x) * 4 + 2] = contents[(y * app.canvas.width + x) * 4 + 2];
                                    imageData.data[(y * app.canvas.width + x) * 4 + 3] = contents[(y * app.canvas.width + x) * 4 + 3];
                                }
                            }
                            app.ctx.putImageData(imageData, 0, 0, 0, 0, app.canvas.width, app.canvas.height);
                            dataStream.close();
                            app.canvasLoaded = true;
                        })
                    });
                }
            })
        });
    }

    app.writeCanvasToFile = function () {
        var roamingFolder = Windows.Storage.ApplicationData.current.roamingFolder;
        roamingFolder.createFileAsync(app.ROAMING_CANVAS_FILENAME, Windows.Storage.CreationCollisionOption.replaceExisting).then(function (file) {
            file.openAsync(Windows.Storage.FileAccessMode.readWrite).then(function (stream) {
                // Copy the canvas image data to an in-memory stream
                var size = app.ctx.canvas.width * app.ctx.canvas.height * 4;
                var imageData = app.ctx.getImageData(0, 0, app.canvas.width, app.canvas.height);

                var dataStream = new Windows.Storage.Streams.InMemoryRandomAccessStream();
                var dataWriter = new Windows.Storage.Streams.DataWriter(dataStream.getOutputStreamAt(0));

                dataWriter.writeBytes(imageData.data);
                dataWriter.storeAsync().then(function () {
                    dataWriter.flushAsync().then(function () {
                        dataStream.seek(0);
                        var outputStream = stream.getOutputStreamAt(0);
                        var compressor = new Windows.Storage.Compression.Compressor(outputStream);
                        Windows.Storage.Streams.RandomAccessStream.copyAsync(dataStream, compressor).then(function () {
                            compressor.finishAsync().then(function () {
                                compressor.detachStream();
                                compressor.close();
                                outputStream.flushAsync().then(function () {
                                    outputStream.close();
                                    app.checkCanvasSizeAgainstQuota();
                                })
                            })
                        })
                    })
                })
            })
        });
    }

    // Because IStream is just way too complicated for WinRT - there's no easy way I can find to query the size of the compressed stream
    // as I write it out above.  There may be some convoluted way to get it, but I haven't found it yet.  IStream::Stat() would be nice, or
    // in JS at least exposing the 'size' property on all stream objects - but noooooooooo. :P
    // So I'll just waste some CPU and IO checking the file size after it's been closed.
    app.checkCanvasSizeAgainstQuota = function () {
        var roamingFolder = Windows.Storage.ApplicationData.current.roamingFolder;
        roamingFolder.getFileAsync(app.ROAMING_CANVAS_FILENAME).then(function (file) {
            file.getBasicPropertiesAsync().then(function (basicProperties) {
                var quotaInBytes = Windows.Storage.ApplicationData.current.roamingStorageQuota * 1024;
                if (basicProperties.size / quotaInBytes > 0.8) {
                    // Approaching the total quota limit, so show a warning
                    id('canvasCompressedDataSize').innerHTML = '<p>Warning: compress canvas setting data is ' + '\"' +
                        basicProperties.size + '\"' + ' Bytes (Total quota is ' + Windows.Storage.ApplicationData.current.roamingStorageQuota + ' KB)</p>';
                }
                else {
                    // Clear the warning out
                    id('canvasCompressedDataSize').innerHTML = '<p>Canvas saved: \"' + basicProperties.size + '\"' + ' Bytes (compressed)</p>';
                }
            })
        });
    }

    app.start();
})();